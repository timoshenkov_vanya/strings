package org.example;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите строку:");
        String loremOop = in.nextLine();
        String[] arr = loremOop.split("object-oriented programming");
        String newLine = "";

        // сплитует строку по нужным строкам и меняет каждую вторую(в данном случае работает на четности)
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 1 & !(arr[i].isEmpty())) {
                newLine += "OOP" + arr[i];
            } else if (i % 2 == 0 & !(arr[i].isEmpty())) {
                newLine += "object-oriented programming" + arr[i];
            }
        }
        // сплитует строку по пробелам, даже если пробелы неодинарные
        String[] secondArr = newLine.split("\\s+");
        int min = 100000;
        String answer = "";


        int unic = 0; // служит для подсчета уникальных значений символов
        for (String i : secondArr) {
            char[] str = i.toCharArray(); // строка переводится в массив из букв
            char[] copy = new char[str.length]; // тут создается массив, куда суются уже проверенные элементы
            int k = 0;
            int count = 0;
            for (char x : str) {
                boolean isInArr = false;

                for (int element : copy) {
                    if (element == x) {
                        isInArr = true;
                    }
                }
                if (!isInArr & Character.isLetter(x)) {
                    for (int g = 0; g < str.length; g++) {
                        if (x == str[g]) {
                            count += 1;
                        }
                    }
                    count -= 1;
                }
                unic = i.length() - count;
                copy[k] = x;
                k += 1;
            }
            if (unic < min) {
                min = unic;
                answer = i;
            }
            unic = 0;
        }

        System.out.println("Слово с наименьшим кол-вом разных символов: " + answer);

        // поиск слов без кириллицы
        int countLatin = 0;
        for (String i : secondArr) {
            char[] str = i.toCharArray();
            boolean isLatin = false;
            for (char x : str) {
                if (x < 1000) {
                    isLatin = true;
                } else {
                    isLatin = false;
                    break;
                }

            }
            if (isLatin) {
                countLatin += 1;
            }
        }
        System.out.println("Слов на латинице: " + countLatin);

        // поиск палиндромов
        for (String i : secondArr) {
            char[] str = i.toCharArray();
            char[] strLeft = new char[i.length()];
            char[] strRight = new char[i.length()];
            for (int x = 0; x < str.length; x++) {
                strLeft[x] = str[x];
                strRight[x] = str[(str.length - 1) - x];
            }
            String left = new String(strLeft);
            String right = new String(strRight);
            if (left.equals(right)) {
                System.out.println("Палиндром: " + i);
            }
        }
    }
}



